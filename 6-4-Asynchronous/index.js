// // AWAL

// const a = () => {
//     setTimeout( () => {
//         console.log('ini a')
//     },2000)
// }

// const b = () => {
//     setTimeout( () => {
//         console.log('ini b')
//     },1000)
// }

// // const c = () => {
// //     setTimeout( () => {
// //         console.log('ini c')
// //     },500)
// // }

// a()
// b()
// // c()


///////////////////////////////////
// CALLBACK

// const a = (callback) => {
//     setTimeout( () => {
//         console.log('ini a')
//         callback("ini response")
//     },2000)
// }

// const b = () => {
//     setTimeout( () => {
//         console.log('ini b')
//     },1000)
// }

// // const c = () => {
// //     setTimeout( () => {
// //         console.log('ini c')
// //     },500)
// // }

// a( (response) => {
//     console.log(response)
//     b()
// })
// //b()
// // c()

// /////////////////////////////////////
// // STUDI KASUS

// const posts = [
//     {
//       title: "Post one",
//       body: "This is post one"
//     },
//     {
//       title: "Post two",
//       body: "This is post two"
//     }
//   ]
  
//   const createPost = (post,callback) => {
//     setTimeout(() => {
//       posts.push(post)
//       callback()
//     }, 2000)
//   }
  
//   const getPosts = () => {
//     setTimeout(() => {
//       posts.forEach(post => {
//         console.log(post)
//       })
//     }, 1000)
//   }
  
//   const newPost = {
//     title: "Post three",
//     body: "This is post three"
//   }
  
//   createPost(newPost, () => {
//     getPosts()
//   })
  

  ////////////////////////////////////////////////
  /// MENGGUNAKAN PROMISE

//   const promise1 = new Promise((resolve, reject) => {
//       setTimeout(() => {
//         let kondisi = "bagus"
//         if(kondisi === "bagus") {
//             resolve("yes")
//         } else {
//             reject("no")
//         }
//       }, 3000)
//   })

//   promise1
//   .then( (response) => {
//       console.log(response)
//   })
//   .catch( (error) => {
//       console.log(error)
//   })

  /////////////////////////
  // STUDI KASUS

  /////////////////////////////////////
// STUDI KASUS

// const posts = [
//     {
//       title: "Post one",
//       body: "This is post one"
//     },
//     {
//       title: "Post two",
//       body: "This is post two"
//     }
//   ]
  
// const createPost = post => {
//     return new Promise( (resolve, reject) => {
//         setTimeout(() => {
//             const koneksi = "bagus"
//             if(koneksi=="bagus") {
//                 posts.push(post)
//                 resolve("berhasil")
//             } else {
//                 reject("gagal")
//             }   
//           }, 2000)
//     })
    
//   }
  
//   const getPosts = () => {
//     setTimeout(() => {
//       posts.forEach(post => {
//         console.log(post)
//       })
//     }, 1000)
//   }
  
//   const newPost = {
//     title: "Post three",
//     body: "This is post three"
//   }
  
//   createPost(newPost)
//   .then( (resp) => {
//     console.log(resp)
//     getPosts()
//   })
//   .catch( error => {
//     console.log(error)
//   })

///////////////////////////////
// async - await


const posts = [
    {
       title:"Post one",
       body:"This is post one"
    },
    {
       title:"Post two",
       body:"This is post two"
    }
 ]
 
 const createPost = post => {
    return new Promise( (resolve, reject) => {
        setTimeout( () => {
           posts.push(post)
           const error = false
           if(!error) {
              resolve()
           }else{
              reject()
           }
        }, 2000)
      })
 }
 
 const getPosts = () => {
      setTimeout(() => {
         posts.forEach(post => {
            console.log(post)
          })
      }, 1000)
 }
 
 const newPost = {
    title:"Post three",
    body: "This is post three"
 }
 
 const init = async() => { 

    try {
        await createPost(newPost)
      getPosts()
        
    } catch (error) {
        console.log("error")
    }
      
 }
 
 init()
 