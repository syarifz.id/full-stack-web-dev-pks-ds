<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('roles')->insert([
            'id'            => '890c45de-9152-4679-868c-635a04c31ca5',
            'name'          => 'author',
            'created_at'    => '2022-03-27 09:50:22',
            'updated_at'    => '2022-03-27 09:50:22'
        ]);


        DB::table('roles')->insert([
            'id'            => '96bd07f2-9f74-4f9e-81b7-dcd25bd8e667',
            'name'          => 'user',
            'created_at'    => '2022-03-27 09:49:42',
            'updated_at'    => '2022-03-27 09:49:42'
        ]);
        DB::table('roles')->insert([
            'id'            => 'c07ce5aa-8479-4a0e-bb6e-88837ce43770',
            'name'          => 'owner',
            'created_at'    => '2022-03-27 09:50:45',
            'updated_at'    => '2022-03-27 09:50:45'
        ]);
        DB::table('roles')->insert([
            'id'            => 'd73a07cb-7e77-4381-8bee-4072786a09f9',
            'name'          => 'admin',
            'created_at'    => '2022-03-27 09:49:45',
            'updated_at'    => '2022-03-27 09:49:45'
        ]);
    
    
    
    }
}


