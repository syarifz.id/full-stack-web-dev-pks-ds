<?php

namespace App\Listeners;

use App\Events\OtpCodeStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\OtpCode;
use App\User;
use App\Mail\OtpCodeMail;
use Illuminate\Support\Facades\Mail;

class SendEmailToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeStoredEvent  $event
     * @return void
     */
    public function handle(OtpCodeStoredEvent $event)
    {
        Mail::to($event->otpCode->user->email)
                    ->send(new OtpCodeMail($event->otpCode, $event->newUserStatus));
    }
}
