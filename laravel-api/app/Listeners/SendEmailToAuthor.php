<?php

namespace App\Listeners;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Post;
use App\Mail\PostCreatedmail;
use App\Events\PostCreated;
use Illuminate\Support\Facades\Mail;


class SendEmailToAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Post $post)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostCreated  $event
     * @return void
     */
    public function handle(PostCreated $event)
    {
        //dd($event->post->user->email);
        Mail::to($event->post->user->email)->send(new PostCreatedMail($event->post));
    }
}
