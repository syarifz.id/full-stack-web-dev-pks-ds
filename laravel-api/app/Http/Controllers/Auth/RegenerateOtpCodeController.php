<?php

namespace App\Http\Controllers\Auth;

use App\Events\OtpCodeStoredEvent;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //dd('test');
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();
        if($user->OtpCode) {
            $user->OtpCode->delete();
        }

        do {
            $otp = mt_rand(100000,999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while($check);

        $validUntil = Carbon::now()->addMinutes(15);
        
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        event(new OtpCodeStoredEvent($otp_code));

        return response()->json([
            'success' => true,
            'message' => 'OTP Regenerated',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code,
            ]
        ]);

    }
}
