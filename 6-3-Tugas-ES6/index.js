// soal 1 .... jawaban soal 1

const kelilingPersegiPanjang = (l,p) => {
    let keliling = 2 * (l+p);
    console.log(keliling);
}

const luasPersegiPanjang = (l,p) => {
    let luas = l*p;
    console.log(luas);
}

kelilingPersegiPanjang(2,3);
luasPersegiPanjang(2,3);


// soal 2 .... jawaban soal 2

const newFunction = literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => console.log(`${firstName} ${lastName}`)
    }
}
    
   
//Driver Code 
newFunction("William", "Imoh").fullName()


// soal 3 .... jawaban soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)


// soal 4 .... jawaban soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]


//Driver Code
console.log(combined)

// soal 5 .... jawaban soal 5

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after)