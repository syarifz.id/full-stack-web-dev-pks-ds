<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi() {
        echo "{$this->nama} sedang {$this->keahliah}";
    }

}

abstract Class Fight {
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hewan) {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br />";
        $hewan->diserang($this);
    }

    public function diserang($hewan) {
        echo "{$this->nama} sedang diserang {$hewan->nama}";
        echo "<br />";
        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
    }

    protected function getInfo() {
        echo "Nama : {$this->nama}";
        echo "<br />";
        echo "Jumlah kaki : {$this->jumlahKaki}";
        echo "<br />";
        echo "Keahlian : {$this->keahlian}";
        echo "<br />";
        echo "Attack power : {$this->attackPower}";
        echo "<br />";
        echo "Defense Power : {$this->defencePower}";
        echo "<br />";
        echo "Darah : {$this->darah}";
        echo "<br />";
    }

    abstract public function getInfoHewan(); 
}

class Elang extends Fight {
    
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "Terbang Tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang";
        echo "<br />";
        
        $this->getInfo();

    }
}

class Harimau extends Fight {
    
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "Lari Cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau";
        echo "<br />";
        
        $this->getInfo();
    }
}

class Spasi {
    public static function tampilkan() {
        echo "<br />======================<br />";
    }
}




$elang = new Elang("Elang Jawa");
$elang->getInfoHewan();
Spasi::tampilkan();
$harimau = new Harimau("Harimau Sumatra");
$harimau->getInfoHewan();
Spasi::tampilkan();



$harimau->serang($elang);
Spasi::tampilkan();

$elang->getInfoHewan();
Spasi::tampilkan();

$harimau->getInfoHewan();
Spasi::tampilkan();

$elang->serang($harimau);
Spasi::tampilkan();

$elang->getInfoHewan();
Spasi::tampilkan();

$harimau->getInfoHewan();
Spasi::tampilkan();




