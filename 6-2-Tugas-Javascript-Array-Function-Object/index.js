
// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
console.log(daftarHewan);


// Soal 2
function introduce(datas) {
    return "Nama saya "+ datas.name +", umur saya "+ datas.age +" tahun, alamat saya di "+ datas.address +", dan saya punya hobby yaitu "+ datas.hobby +"!";
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) 

// Soal 3

function hitung_huruf_vokal(str) {

    var jumlahVokal = 0;
    var kata = str.toString();
    for (var i = 0; i <= kata.length - 1; i++) {
        if (kata.charAt(i) == "a" || kata.charAt(i) == "e" || kata.charAt(i) == "i" || kata.charAt(i) == "o" || kata.charAt(i) == "u") {
            jumlahVokal += 1;
          }
    }
    return jumlahVokal;

}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// Soal 4

function hitung(i) {
    i = i + (i- 2);
    return i;
}

console.log( hitung(0) ) // -2   0 + -2
console.log( hitung(1) ) // 0    1 + -1
console.log( hitung(2) ) // 2    2 + 0 
console.log( hitung(3) ) // 4    3 + 1
console.log( hitung(5) ) // 8    4 + 2 
                         //      5 + 3



